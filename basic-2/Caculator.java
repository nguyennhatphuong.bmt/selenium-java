// https://www.w3schools.com/java/java_data_types_numbers.asp
public class Caculator {
    // instance variables
    private int a;
    private byte b;

    private float c;
    private double d;

    // constructor
    private Caculator() {
        a = 1;
        b = 2;

        c = 0.1f;
        d = 2d;
    }

    public static int caculateInt(int a, byte b) {
        return a + b;
    }

    public static double caculateDouble( float c, double d  ) {
        // https://www.w3schools.com/java/java_type_casting.asp
        return (double) c * (float) d;
    }

    public static void main(String[] args) {
        Caculator classInstance = new Caculator();

        System.out.print( "The sum of 2 integers is: " + caculateInt( classInstance.a, classInstance.b ) + ". " );

        System.out.print( "The multiplies of 2 fractional numbers is: " + caculateDouble( classInstance.c, classInstance.d) );
    }
}
