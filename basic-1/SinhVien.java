public class SinhVien {
    // static variable
    private static String info = "Unknown info";

    // Testing variable types (static vs instance)
    public static int initCounter = 0;
    int initValue = 0;

    // instance variable
    private String fullName;

    // https://www.w3schools.com/java/java_constructors.asp
    // constructor
    public SinhVien() {
        // Set the initial value
        fullName = "Unknown";

        // Init the info text
        this.setInfo(fullName);

        // Increase the number
        initCounter++;
        initValue++;
    }

    // setter
    public void setInfo(String fullName) {
        // Don't change the fullName when it's empty
        if (!fullName.isEmpty()) {
            // https://www.w3schools.com/java/ref_keyword_this.asp
            this.fullName = fullName;
        }

        info = "This is student " + this.fullName;
    }

    // https://www.w3schools.com/java/java_encapsulation.asp
    // getter
    public String getInfo() {
        return info;
    }

    // https://www.w3schools.com/java/java_syntax.asp
    public static void main(String[] args) {
        // print original info from static variable (without getter)
        System.out.println(info);

        // create class instances
        SinhVien sinhVienInstance = new SinhVien();
        ThongTin thongTinInstance = new ThongTin();

        // https://www.freecodecamp.org/news/static-variables-in-java
        // Testing static variable vs. instance variable
        SinhVien anotherSinhVienInstance = new SinhVien();
        System.out.print( "Class created " + initCounter + " times. The initial value is " + anotherSinhVienInstance.initValue + ". " );

        // set the new info (with setter)
        sinhVienInstance.setInfo("Nguyen Nhat Phuong");

        // local variable are assigned from static variable (in the current class) (with getter)
        String currentInfo = sinhVienInstance.getInfo();

        // local variable are assigned from static variable (in another class) (without getter)
        String currentLocation = thongTinInstance.location;

        // print the new value (from local variables)
        System.out.println(currentInfo + " living in " + currentLocation);
    }
}
